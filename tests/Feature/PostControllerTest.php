<?php

namespace Tests\Feature;

use Mockery;
use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostControllerTest extends TestCase
{
    public function testAll()
    {
        $this->verifyMethodCall(PostRepositoryInterface::class, 'all');

        $response = $this->get('posts');

        $response->assertOk();
        $response->assertViewHas('posts');
    }

    public function testStore()
    {
        $this->verifyMethodCall(PostRepositoryInterface::class, 'store');

        $response = $this->post('posts');

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('posts.index'));
    }

    public function verifyMethodCall(string $namespace, string $methodName)
    {
        // Mocks the specified class.
        $mock = Mockery::mock($namespace);

        // Verify the specified method call.
        $mock->expects()->$methodName()->once();

        // Bind the mock into the container
        $this->app->instance($namespace, $mock);
    }
}
