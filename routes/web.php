<?php

// class Customer
// {
//     protected $order;

//     public function __construct(Order $order)
//     {
//         $this->order = $order;
//     }
// }

// class Order
// {
// }

// app()->bind('Customer', function () {
//     return new Customer(new Order);
// });

// Still works without the binding after first bind.
// Route::get('customers', function () {
//     dd(app()->make('Customer'));
// });

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

app()->bind(
    'App\Repositories\PostRepositoryInterface',
    'App\Repositories\EloquentPostRepository'
);

Route::resource('posts', 'PostController');
